let id;
let Items;
$(document).ready(function () {
    loadLists();
});

function loadLists() {
    const endpoint_lists = 'https://zaanlist.herokuapp.com/zaan/todolists';

    fetch(endpoint_lists)
        .then(response => response.json())
        .then(llistes => renderLists(llistes));
}

function renderLists(llistes) {
    llistes.forEach(function (llista) {
        renderListTitles(llista);
    })
}

function renderListTitles(llista) {
    const div = document.createElement('div')
    div.id = (llista.idLista)
    div.classList.add('divList')
    document.body.appendChild(div)

    const lapiz = document.createElement("SPAN");
    const txt2 = document.createTextNode("\u270E");

    lapiz.className = "editList";
    lapiz.appendChild(txt2);
    div.appendChild(lapiz);

    const cruz = document.createElement("SPAN");
    const txt = document.createTextNode("\u00D7");

    cruz.className = "closeList";
    cruz.appendChild(txt);
    div.appendChild(cruz);

    const h2 = document.createElement("h2")
    const t = document.createTextNode(llista.listaName);
    h2.appendChild(t);
    document.getElementById(llista.idLista).appendChild(h2);



    //eliminar lista
    cruz.addEventListener('click', function () {
        deleteLista(llista.idLista)
        const div = this.parentElement;
        div.style.display = "none";
    })



    //editar nombre lista
    lapiz.addEventListener('click', function () {
        id = llista.idLista
        guardarItems()
        showEdit()
    })

    const ul = document.createElement("ul")
    document.getElementById(llista.idLista).appendChild(ul);

    loadItemsIndex(llista)

    //entrar a la lista seleccionada
    h2.addEventListener('click', function () {
        window.location = ('./list.html?idLista=' + llista.idLista)
    });
    ul.addEventListener('click', function () {
        window.location = ('./list.html?idLista=' + llista.idLista)
    });

}

//creacion 1 lista
function newList() {
    const inputValue = document.getElementById("myInputList").value;
    if (inputValue !== '') {
        const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todolists';
        const data = {listaName: inputValue};
        fetch(endpoint_list, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(() => $('.divList').remove())
            .then(listas => loadLists(listas))
            .catch(error => console.error('Error:', error));
    }
    document.getElementById("myInputList").value = "";
    document.getElementById("addList").style.display = "none";
}

// //editar nombre lista
function editListName() {
    const inputValue = document.getElementById("myEdit").value;
    if (inputValue !== '') {
        const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todolists';
        const data = {idLista: id, listaName: inputValue};
        fetch(endpoint_list, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(() => updateItems())
            .then(() => $('.divList').remove())
            .then(listas => loadLists(listas))
            .catch(error => console.error('Error:', error));
    }

    document.getElementById("myEdit").value = "";
    document.getElementById("editList").style.display = "none";
}

//guardar items para actualizar nombre lista
function guardarItems() {
    const endpoint_Items = 'https://zaanlist.herokuapp.com/zaan/todolists/' + id + '/todoitems';
    fetch(endpoint_Items)
        .then(response => response.json())
        .then(items => Items = items )
}

//añadir items guardados a la lista tras editar nombre
function updateItems() {
    const endpoint_item = 'https://zaanlist.herokuapp.com/zaan/todoitems/';
    Items.forEach(function (item) {
        const data = {id: item.id, itemName: item.itemName, lista: {idLista: id}, hecho: item.hecho};
        fetch(endpoint_item, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    })
}

//eliminar lista
function deleteLista(id) {
    const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todolists/' + id;
    fetch(endpoint_list, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(() => $('.divList').remove())
        .then(listas => loadLists(listas));
}

//ITEMS
function loadItemsIndex(llista) {
    const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todolists/' + llista.idLista + '/todoitems';
    fetch(endpoint_list)
        .then(response => response.json())
        .then(items => renderItemsIndex(items, llista.idLista));
}

function renderItemsIndex(items, idLista) {
    for (let i = 0; i < 3; i++) {
        renderItemIndex(items[i], idLista)
    }
}

function renderItemIndex(item, idLista) {
    const li = document.createElement("li");
    const t = document.createTextNode(item.itemName);
    li.appendChild(t);
    document.getElementById(idLista).getElementsByTagName("ul")[0].appendChild(li);
}

//funcionalidad

// Boton añadir
function showDiv() {
    if (document.getElementById("addList").style.display === 'none') {
        document.getElementById("addList").style.display = 'block';
        document.getElementById("editList").style.display = 'none';
    } else {
        document.getElementById("addList").style.display = 'none';
    }
}

function showEdit() {
    if (document.getElementById("editList").style.display === 'none') {
        document.getElementById("editList").style.display = 'block';
        document.getElementById("addList").style.display = 'none';
    } else {
        document.getElementById("editList").style.display = 'none';
    }
}
