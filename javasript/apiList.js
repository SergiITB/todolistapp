let queryString;
let urlParams;
let id;
let idItem;
let hecho;
$(document).ready(function () {
    queryString = window.location.search;
    urlParams = new URLSearchParams(queryString);
    id = urlParams.get('idLista')
    loadLists();
    titleList();
    loadItems();
});

// listado listas
function loadLists() {
    const endpoint_lists = 'https://zaanlist.herokuapp.com/zaan/todolists';
    fetch(endpoint_lists)
        .then(response => response.json())
        .then(llistes => renderLists(llistes)).catch(function () {
        alert("No es poden obtenir les llistes, torna a provar-ho");
    });
}

function renderLists(llistes) {
    llistes.forEach(function (llista) {
        renderListTitles(llista);
    })
}

function renderListTitles(llista) {
    const h2 = document.createElement("h2")
    const t = document.createTextNode(llista.listaName);
    h2.appendChild(t);

    document.getElementById("listsDiv").appendChild(h2);

    h2.addEventListener('click', function () {
        window.location = ('./list.html?idLista=' + llista.idLista)
    });
}

//titulo lista actual
function titleList() {
    const endpoint_lists = 'https://zaanlist.herokuapp.com/zaan/todolists/' + id;
    fetch(endpoint_lists)
        .then(response => response.json())
        .then(lista => Lista(lista)).catch(function () {
        alert("No se encuentra el titúlo");
    });
}

function Lista(lista) {
    const h2 = document.createElement("h2")
    const t = document.createTextNode(lista.listaName);
    h2.appendChild(t);
    document.getElementById("myDIV").appendChild(h2);
}

// Aqui empieza Items
function loadItems() {
    const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todolists/' + id + '/todoitems';
    fetch(endpoint_list)
        .then(response => response.json())
        .then(data => renderItems(data))
}

function renderItems(items) {
    items.forEach(function (item) {
        renderItem(item)
    })
}

function renderItem(item) {
    const li = document.createElement("li");
    const t = document.createTextNode(item.itemName);
    li.appendChild(t);
    document.getElementById("objectsDiv").getElementsByTagName("ul")[0].appendChild(li)

    const cruz = document.createElement("SPAN");
    const txt = document.createTextNode("\u00D7");

    cruz.className = "close";
    cruz.appendChild(txt);
    li.appendChild(cruz);

    const lapiz = document.createElement("SPAN");
    const txt2 = document.createTextNode("\u270E");

    lapiz.className = "edit";
    lapiz.appendChild(txt2);
    li.appendChild(lapiz);

    cruz.addEventListener('click', function () {
        deleteItem(item.id)
        const div = this.parentElement;
        div.style.display = "none";
    })

    lapiz.addEventListener('click', function () {
        idItem = item.id
        hecho = item.hecho
        showEdit()
    })

    li.addEventListener("click", function () {
        updateTaskStatus(item)
    })

    if (item.hecho === true) {
        li.className = "checked"
    }
}

// Create a new list item when clicking on the "Add" button
function newElementItem() {
    const inputValue = document.getElementById("myInput").value;
    if (inputValue !== '') {
        const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todoitems';
        const data = {itemName: inputValue, lista: {idLista: id}, hecho: false};
        fetch(endpoint_list, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(() => $(document.getElementsByTagName('li')).remove())
            .then(data => loadItems(data))
            .catch(error => console.error('Error:', error));
    }
    document.getElementById("myInput").value = "";
    document.getElementById("add").style.display = "none";
}

//edit item name
function editItemName() {
    const inputValue = document.getElementById("myEdit").value;
    if (inputValue !== '') {
        const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todoitems/';
        const data = {id: idItem, itemName: inputValue, lista: {idLista: id}, hecho: hecho};
        fetch(endpoint_list, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(() => $(document.getElementsByTagName('li')).remove())
            .then(data => loadItems(data))
            .catch(error => console.error('Error:', error))
    }
    document.getElementById("myEdit").value = "";
    document.getElementById("edit").style.display = "none";
}

//marcar como hecho o no hecho
function updateTaskStatus(item) {
    item.hecho = !item.hecho
    const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todoitems/';
    console.log(JSON.stringify(item));
    fetch(endpoint_list, {
        method: 'PUT',
        body: JSON.stringify(item),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(() => $(document.getElementsByTagName('li')).remove())
        .then(data => loadItems(data))
        .catch(error => console.error('Error:', error));
}

//eliminar item
function deleteItem(idItem) {
    const endpoint_list = 'https://zaanlist.herokuapp.com/zaan/todoitems/' + idItem;
    fetch(endpoint_list, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(() => $(document.getElementsByTagName('li')).remove())
        .then(() => $(document.getElementsByClassName('DivList')).remove())
        .then(() => $(document.getElementsByClassName('checked')).remove())
        .then(data => loadItems(data))
        .catch(error => console.error('Error:', error));
}

//Funciones
// Boton del plus
function showDiv() {
    if (document.getElementById("add").style.display === 'none') {
        document.getElementById("add").style.display = 'block';
        document.getElementById("edit").style.display = 'none';
    } else {
        document.getElementById("add").style.display = 'none';
    }
}

function showEdit() {
    if (document.getElementById("edit").style.display === 'none') {
        document.getElementById("edit").style.display = 'block';
        document.getElementById("add").style.display = 'none';
    } else {
        document.getElementById("edit").style.display = 'none';
    }
}

// Add a "checked" symbol when clicking on a list item
const list = document.querySelector('ul');
list.addEventListener('click', function (ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);

//menu lateral
function ocultarListsDiv() {
    if (document.getElementById("listsDiv").style.display === "none") {
        document.getElementById("listsDiv").style.display = "block"
        document.getElementById("listsDivTitle").style.display = "block"
        document.getElementById("objectsDiv").style.width = "67%"
        document.getElementById("myDIV").style.width = "67%"
        document.getElementById("add").style.width = "57%"
        document.getElementById("edit").style.width = "57%"
    } else {
        document.getElementById("listsDiv").style.display = "none"
        document.getElementById("listsDivTitle").style.display = "none"
        document.getElementById("objectsDiv").style.width = "97%"
        document.getElementById("myDIV").style.width = "97%"
        document.getElementById("add").style.width = "87%"
        document.getElementById("edit").style.width = "87%"
    }
}