// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    myNodelist[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
    }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);

// Boton del plus  - añadido api.js
function showDiv() {
    document.getElementById(    "add").style.display='block';
}

function showDiv2() {
    document.getElementById("add2").style.display = 'block';
}

// Create a new list item when clicking on the "Add" button
function newElement() {
    const li = document.createElement("li");
    li.draggable = true;
    const inputValue = document.getElementById("myInput").value;
    const t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue !== '') {
        document.getElementById("myUL").appendChild(li);
    }
    document.getElementById("myInput").value = "";

    const span = document.createElement("SPAN");
    const txt = document.createTextNode("\u00D7");

    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            const div = this.parentElement;
            div.style.display = "none";
        }
    }
    document.getElementById("add").style.display="none";
}
//creacion 1 lista - añadido a api.js
let contador = 0
function newList() {

    const div1 = document.createElement('div')
    document.body.appendChild(div1)
    div1.classList.add('divList')


    const h2 = document.createElement("h2")
    const inputValue = document.getElementById("myInputList").value;
    const t = document.createTextNode(inputValue);
    h2.appendChild(t)
    if (inputValue !== '') {
        //se añade al div num [x]
        document.getElementsByClassName("divList")[contador +1].appendChild(h2);
        contador+=1
    }
    document.getElementById("myInputList").value = "";

    document.getElementById("add2").style.display="none";
}